#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv[]) {
    char * const cat_args[] = { "chroot", "/var/vsys/bookworm", "/usr/bin/scanimage",
        "--resolution", argv[1],
        "-x", argv[2],
        "-y", argv[3],
        "--mode", "Color",
        "--format", argv[4],
        NULL };

    execvpe("/usr/sbin/chroot", cat_args, NULL);
}
