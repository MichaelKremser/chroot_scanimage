# Rationale

In Debian 11 bullseye, `scanimage` is partly broken. With my Canon PIXMA TS 3450 an "Out of memory" exception occurs. This is due to a bug in version 1.0.31 that was solved in version 1.0.32.

However, in the upcoming version Debian 12 bookworm, the bug is solved, but the version of `scanimage` that ships with bookworm does not work on Debian 11. That's why I installed it in a `chroot` environment and mounted `proc`, `devpts`, `dev` and `sys` to that environment. This way, `scanimage` works, but it could only be used by user root.

Linux provides a solution to this: "set user or group ID on execution". If this attribute is set on an executable file, the program is run under the credentials of the file's owner. That means, if that program is owned by root and the attribute is set and a normal user runs the program, the program runs with root's privileges. Sounds scary at first, but for example, the `passwd` program also uses this technology.

# Installation

Run these commands in the directory with the binary that you have created by invoking `make`:

```
chown root.root chroot_scanimage && chmod u+s chroot_scanimage && chmod g+s chroot_scanimage && chmod o+x chroot_scanimage
```

# Running

`chroot_scanimage` **Resolution** **Width** **Height** **Format**

Example:

`chroot_scanimage` 300 216 297 pdf

# Conclusion

This program is tailored completely to **my** needs. Most possibly, nobody except me will have the need to do this (the beforementioned bug only affects certain scanner models). When compiling, a lot of warnings occur that I did not fix because this is just a **temporary** solution. As soon as Debian 12 is available as a stable version, the affected system will be upgraded and there will be no more need for this workaround.